# TerminalCraft
# Copyright (C) 2021  Finian Wright
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import curses
from math import floor
import time
from random import randint

def timeMinigame(speed, accuracy=10):
	screen=curses.initscr()
	rows, screenCols = screen.getmaxyx()
	cols=80
	screen.keypad(True)
	curses.curs_set(False)
	screen.addstr(0, 0, "Press enter when progress bar is next to the star", curses.A_STANDOUT)
	curses.halfdelay(1)
	lastMove=time.time()
	position=0
	curses.start_color()
	curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_GREEN)
	curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLACK)
	for col in range(cols-1):
		if col==40:
			screen.addstr(floor(rows/2), floor(cols/2), "*", curses.color_pair(1))
		elif col > 40-accuracy and col < 40+accuracy:
			screen.addstr(floor(rows/2), col, "-", curses.color_pair(2))
		else:
			screen.addstr(floor(rows/2), col, "-")
	while True:
		if position==80:
			break
		if screen.getch()==10:
			break
		for col in range(0, int(position)):
			screen.addch(floor(rows/2)+1, col, "=")
		position+=speed
		lastMove=time.time()
		screen.refresh()
	screen.clear()
	screen.refresh()
	screen.keypad(False)
	curses.endwin()
	curses.curs_set(True)
	return position-40

def attackMinigame(speed, accuracy=10, message="Press enter when the arrow is next to the star"):
	screen=curses.initscr()
	rows, screenCols = screen.getmaxyx()
	cols=80
	screen.keypad(True)
	curses.curs_set(False)
	screen.addstr(0, 0, message, curses.A_STANDOUT)
	curses.halfdelay(1)
	lastMove=time.time()
	position=0
	curses.start_color()
	curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_GREEN)
	curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLACK)
	## vvv I don't know why, but if you remove it, the game breaks vvv
	accuracy = int(accuracy)
	for col in range(cols-1):
		if col==40:
			screen.addstr(floor(rows/2), floor(cols/2), "*", curses.color_pair(1))
		elif col > 40-accuracy and col < 40+accuracy:
			screen.addstr(floor(rows/2), col, "-", curses.color_pair(2))
		else:
			screen.addstr(floor(rows/2), col, "-")
	while True:
		if position<=-1:
			speed=-speed
			position=0
		if position>=79:
			speed=-speed
			position=78
		for col in range(cols-1):
			if col!=position:
				screen.addch(floor(rows/2)+1, col, " ")
			else:
				screen.addch(floor(rows/2)+1, col, "^")
		if screen.getch()==10:
			break
		lastMove=time.time()
		screen.refresh()
		position+=speed
	screen.clear()
	screen.refresh()
	screen.keypad(False)
	curses.endwin()
	curses.curs_set(True)
	return position-40

def fishingMinigame(speed, accuracy=10, message="Press enter when the fish is next to the star"):
	screen=curses.initscr()
	rows, screenCols = screen.getmaxyx()
	cols=80
	screen.keypad(True)
	curses.curs_set(False)
	screen.addstr(0, 0, message, curses.A_STANDOUT)
	curses.halfdelay(1)
	lastMove=time.time()
	position=0
	curses.start_color()
	curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_GREEN)
	curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLACK)
	for col in range(cols-1):
		if col==40:
			screen.addstr(floor(rows/2), floor(cols/2), "*", curses.color_pair(1))
		elif col > 40-accuracy and col < 40+accuracy:
			screen.addstr(floor(rows/2), col, "-", curses.color_pair(2))
		else:
			screen.addstr(floor(rows/2), col, "-")
	while True:
		if position<=-1:
			position=0
		if position>=79:
			position=0
		for col in range(cols-1):
			if col!=position:
				screen.addch(floor(rows/2)+1, col, " ")
			else:
				screen.addch(floor(rows/2)+1, col, "🐟")
		if screen.getch()==10:
			break
		lastMove=time.time()
		screen.refresh()
		position+=randint(int(-speed/2), int(speed))
	screen.clear()
	screen.refresh()
	screen.keypad(False)
	curses.endwin()
	curses.curs_set(True)
	return position-40
