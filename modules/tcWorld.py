# TerminalCraft
# Copyright (C) 2021  Finian Wright
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import random
import os
import configparser
import time
import copy
import itertools

def listToString(list):
	string=""
	for item in list:
		string+=str(item)+","
	if len(string)>0:
		if string[-1]==",":
			string=string[:-1]
	if len(string)>0:
		if string[0]==",":
			string=string[1:]
	return string
def listFromString(string):
	newList=[]
	for item in string.split(","):
		if not item.strip() =="":
			newList.append(item)
	return newList

class world():
	map={}
	currentPlayer=None

	## Controls the size of biomes
	biomeSize=10

	class biome():
		## Name of the biome
		name=None
		## Whether plants can be grown in the biome
		farmable=False
		## Whether trees generate in the biome
		forested=False
		## Whether the biome can be cultivated
		grassy=False
		## Whether the biome requires a boat to traverse
		ocean=False
		## Snowy areas cannot be cultivated, however, the snow can be cleared which will make the area grassy instead of snowy
		snowy=False
		## Whether stone can be mined in the biome and the area generates ores
		underground=False
		## Whether the biome generates naturally
		natural=True

	## Plains biome
	plains=biome()
	plains.name="plains"
	plains.grassy=True

	## Forest biome
	forest=biome()
	forest.name="forest"
	forest.forested=True
	forest.grassy=True

	## Ocean biome
	ocean=biome()
	ocean.name="ocean"
	ocean.ocean=True

	## Cave biome
	cave=biome()
	cave.name="cave"
	cave.underground=True

	## Field biome
	field=biome()
	field.name="field"
	field.farmable=True
	field.natural=False

	## Desert biome
	desert=biome()
	desert.name="desert"

	## Taiga biome
	taiga=biome()
	taiga.name="taiga"
	taiga.snowy=True
	taiga.forested=True

	## Snowy plains
	snowPlains=biome()
	snowPlains.name="snowy plains"
	snowPlains.snowy=True

	## List of all biomes
	biomes=[plains, forest, ocean, cave, field, desert, taiga, snowPlains]

	## Path to save world
	path="world.tcworld"

	class area():
		name=""
		type=None
		mobs=[]
		items=[]
		trees=0
		ore=[]
		plants=[]
		mobs=[]
		snowy=False
		grassy=False

	class player():
		def heal(self, amount):
			if self.health+amount>self.maxHealth:
				self.health=self.maxHealth
			else:
				self.health+=amount
		def restoreMana(self, amount):
			if self.mana+amount>self.maxMana:
				self.mana=self.maxMana
			else:
				self.mana+=amount

		location=[0, 0]
		inventory=[]
		health=100
		maxHealth=100
		mana=100
		maxMana=100
		heldWeapon=None
		spells=[""]

	class recipe():
		item=None
		ingredients=None
		time=0

	## Boat recipe
	boat=recipe()
	boat.item="boat"
	boat.ingredients=['wood', 'wood', 'wood', 'wood', 'wood']

	## Crude pickaxe recipe
	crudePickaxe=recipe()
	crudePickaxe.item="crude pickaxe"
	crudePickaxe.ingredients=['wood', 'wood']

	## Pickaxe recipe
	pickaxe=recipe()
	pickaxe.item="pickaxe"
	pickaxe.ingredients=['wood', 'stone']

	## Hoe recipe
	hoe=recipe()
	hoe.item="hoe"
	hoe.ingredients=['wood', 'wood']

	## Shovel recipe
	shovel=recipe()
	shovel.item="shovel"
	shovel.ingredients=['wood', 'wood']

	## Dough recipe
	dough=recipe()
	dough.item="dough"
	dough.ingredients=['wheat', 'wheat', 'wheat']

	## Campfire recipe
	campfire=recipe()
	campfire.item="campfire"
	campfire.ingredients=['wood', 'wood', 'wood', 'flint']

	## Spear recipe
	spear=recipe()
	spear.item="spear"
	spear.ingredients=['wood', 'wood', 'stone']

	## Sword recipe
	sword=recipe()
	sword.item="sword"
	sword.ingredients=['wood', 'iron', 'iron']

	## Wand recipe
	wand=recipe()
	wand.item="wand"
	wand.ingredients=['leather', 'wood', 'goblin tooth']

	## Rod recipe
	rod=recipe()
	rod.item="rod"
	rod.ingredients=['leather', 'bone', 'bone', 'skull']

	## Fishing rod recipe
	fishingRod=recipe()
	fishingRod.item="fishing rod"
	fishingRod.ingredients=['wood', 'string']

	## Beef sandwich
	beefSandwich=recipe()
	beefSandwich.item="beef sandwich"
	beefSandwich.ingredients=['cooked beef', 'bread']

	## Mutton sandwich
	muttonSandwich=recipe()
	muttonSandwich.item="mutton sandwich"
	muttonSandwich.ingredients=['cooked mutton', 'bread']


	recipes=[boat, crudePickaxe, pickaxe, hoe, dough, campfire, spear, sword, shovel, wand, rod, fishingRod, beefSandwich, muttonSandwich]

	## Cooking recipes

	## Bread recipe
	bread=recipe()
	bread.item="bread"
	bread.ingredients=['dough']
	bread.time=2

	## Cooked beef recipe
	cookedBeef=recipe()
	cookedBeef.item="cooked beef"
	cookedBeef.ingredients=["raw beef"]
	cookedBeef.time=4

	## Cooked mutton recipe
	cookedMutton=recipe()
	cookedMutton.item="cooked mutton"
	cookedMutton.ingredients=["raw mutton"]
	cookedMutton.time=4

	cookingRecipes=[bread, cookedBeef, cookedMutton]

	class plant():
		name=None
		fruit=[]
		seed=None

	## Wheat plant
	wheat=plant()
	wheat.name="wheat"
	wheat.fruit="wheat"
	wheat.seed="wheat seed"

	plants=[wheat]

	## Grow plants
	def growPlants(self):
		for x in range(self.currentPlayer.location[0]-5, self.currentPlayer.location[0]+5):
			for y in range(self.currentPlayer.location[1]-5, self.currentPlayer.location[1]+5):
				loadedArea=self.getArea(x,y)
				if len(loadedArea.plants)>0:
					plantName=loadedArea.plants[0]
					if random.randint(0,3)==0:
						for plant in self.plants:
							if plant.name==plantName:
								loadedArea.items.append(plant.fruit)
								for i in range(0, random.randint(2, 4)):
									loadedArea.items.append(plant.seed)
						loadedArea.plants.remove(plantName)

	class weapon():
		name=None
		attack=10
		magic=False
		accuracy=10

	## Spear definition
	spear=weapon()
	spear.name="spear"
	spear.attack=5

	## Sword definition
	sword=weapon()
	sword.name="sword"
	sword.attack=10

	## Wand definition
	wand=weapon()
	wand.name="wand"
	wand.attack=10
	wand.magic=True

	## Rod definition
	rod=weapon()
	rod.name="rod"
	rod.attack=20
	rod.magic=True

	## Staff definition
	staff=weapon()
	staff.name="staff"
	staff.attack=30
	staff.magic=True

	weapons=[spear, sword, wand, rod, staff]

	class spell():
		name=None
		description=None
		requiredPower=0
		cost=0
		heal=0
		attack=0
		escape=False
		learnFrom=""
		learnMessage=""
		messages=["THIS SPELL DOES NOT HAVE A CAST MESSAGE"]

	## Fireball spell
	fireball=spell()
	fireball.name="fireball"
	fireball.description="Shoots a flaming ball from your wand"
	fireball.attack=10
	fireball.cost=5
	fireball.learnFrom="fireball scroll"
	fireball.learnMessage="You read the scroll. After nearly lighting your hair on fire several times you learn how to shoot a fireball from your wand."
	fireball.messages=["You scream a series of unrecognizable noises. Suddenly a flaming sphere shoots out from your wand into your enemy.", "Your wand catches fire. Wait no it seems to be... birthing something? Oh it was shooting a fireball at your enemy. I always forget about those."]

	## Heal spell
	heal=spell()
	heal.name="heal"
	heal.description="Heals your player"
	heal.heal=5
	heal.cost=10
	heal.learnFrom="heal scroll"
	heal.learnMessage="You read the scroll. After reading the instructions and a bit of practice you are now able to heal yourself using your wand."
	heal.messages=["A weird undulating looking lump slowly comes out of your wand. Instead of going towards your enemy, however, it loops around and goes directly into your mouth. Yummy! You recieve 5 health."]

	## Lightning bolt spell
	lightning=spell()
	lightning.name="lightning bolt"
	lightning.description="Strikes your enemy with lightning"
	lightning.requiredPower=20
	lightning.attack=50
	lightning.cost=20
	lightning.learnFrom="eel chunk"
	lightning.learnMessage="You pick up the unidentifiable chunk of eel pieces. After looking through the viscera you find a note explaining how to cast a lightning spell. I guess it was worth it?"
	lightning.messages=["Lightning shoots from the tip of your wand and bounces off the sky, hitting your enemy."]

	## Unknown spell
	unknown=spell()
	unknown.name="unknown"
	unknown.description="I have no idea what this is or how it got here. You probably shouldn't use it."
	unknown.learnFrom="strange rock"
	unknown.learnMessage="You notice some writing on the bottom of the strange rock. You can't read it but somehow understand what you must do. After a while you figure out how to cast a new spell."
	unknown.cost=100
	unknown.messages=["Ignoring my warnings you cast the unknown spell. Nothing happened. Probably."]

	spells=[fireball, heal, unknown, lightning]

	class combatItem():
		name=None
		heal=0
		attack=0
		manaRestore=0
		messages=[]

	## Health potion
	healthPotion=combatItem()
	healthPotion.name="health potion"
	healthPotion.heal=10
	healthPotion.messages=["You drink the viscous red liquid. It tastes terrible but you feel rejuvenated."]

	## Mana potion
	manaPotion=combatItem()
	manaPotion.name="mana potion"
	manaPotion.manaRestore=10
	manaPotion.messages=["This is the brightest blue thing you have ever seen. You worry that looking at it may blind you so drink it as fast as possible. You feel full of magic."]

	## Bread
	breadCombatItem=combatItem()
	breadCombatItem.name="bread"
	breadCombatItem.heal=1
	breadCombatItem.messages=["You pull an entire loaf of bread from somewhere and eat it. 'This would be better combined with something else', you think"]

	## Beef
	beefCombatItem=combatItem()
	beefCombatItem.name="raw beef"
	beefCombatItem.heal=1
	beefCombatItem.messages=["You take a raw lump of beef out of your bag. Still dripping with blood, you shove it into your mouth. Eww."]

	## Mutton
	muttonCombatItem=combatItem()
	muttonCombatItem.name="raw mutton"
	muttonCombatItem.heal=1
	muttonCombatItem.messages=["You pull a lump of something that you're pretty sure is mutton from your pocket and eat it. I wonder if there's some way you could prepare food before eating it."]

	## Cooked beef
	cookedBeefCombatItem=combatItem()
	cookedBeefCombatItem.name="cooked beef"
	cookedBeefCombatItem.heal=3
	cookedBeefCombatItem.messages=["You quickly grab a slab of beef and eat it with your bare hands. 'There must be a better way to eat meat' you think, but it seems to have helped a bit"]

	## Cooked mutton
	cookedMuttonCombatItem=combatItem()
	cookedMuttonCombatItem.name="cooked mutton"
	cookedMuttonCombatItem.heal=3
	cookedBeefCombatItem.messages=["You pull a chunk of mutton out of your back pocket and rip it apart with your teeth. 'I wonder if I could combine this with bread to make it easier to eat', you think."]

	## Beef sandwich
	beefSandwichCombatItem=combatItem()
	beefSandwichCombatItem.name="beef sandwich"
	beefSandwichCombatItem.heal=4
	beefSandwichCombatItem.messages=["You eat the sandwich, yummy!"]

	## Mutton sandwich
	muttonSandwichCombatItem=combatItem()
	muttonSandwichCombatItem.name="mutton sandwich"
	muttonSandwichCombatItem.heal=4
	muttonSandwichCombatItem.messages=["You eat the sandwich, yummy!"]

	combatItems=[healthPotion, manaPotion, breadCombatItem, beefCombatItem, muttonCombatItem, cookedBeefCombatItem, cookedMuttonCombatItem, beefSandwichCombatItem, muttonSandwichCombatItem]

	class mob():
		## Name of mob
		name=None
		## Possible drops
		drops=[]
		## Chances of drops
		dropChances=[]
		## Current health (Do not change when defining, will be reset to maxHealth when combat starts)
		health=0
		## Starting health
		maxHealth=50
		## Maxiumum attack damage
		attack=0
		## Disposition can be "passive", "neutral", or "aggressive"
		disposition="passive"
		## List of biomes the mob can spawn in
		spawnable=["plains"]
		## Whether it's possible to escape the battle
		escapable=True
		## Percent chance of spawning
		spawnRate=20

	## Cow mob
	cow=mob()
	cow.name="cow"
	cow.maxHealth=10
	cow.drops=["leather", "raw beef"]
	cow.dropChances=[80,   100]

	## Sheep mob
	sheep=mob()
	sheep.name="sheep"
	sheep.maxHealth=10
	sheep.drops=["wool", "raw mutton"]
	sheep.dropChances=[80, 100]

	## Goblin mob
	goblin=mob()
	goblin.name="goblin"
	goblin.maxHealth=20
	goblin.attack=5
	goblin.spawnable=["cave"]
	goblin.disposition="neutral"
	goblin.drops=["tattered cloth", "goblin tooth", "mana potion"]
	goblin.dropChances=[70,			 50,			 40]

	## Skeleton mob
	skeleton=mob()
	skeleton.name="skeleton"
	skeleton.maxHealth=50
	skeleton.attack=10
	skeleton.spawnable=["cave"]
	skeleton.disposition="aggressive"
	skeleton.drops=[	"bone", "fireball scroll", "skull", "health potion"]
	skeleton.dropChances=[80,	 20,				10,		 40]
	skeleton.spawnRate=10

	## Fairy mob
	fairy=mob()
	fairy.name="fairy"
	fairy.maxHealth=5
	fairy.attack=1
	fairy.spawnable=["plains", "cave", "forest", "field", "taiga", "snowy plains"]
	fairy.disposition="neutral"
	fairy.drops=["fairy dust", "heal scroll"]
	fairy.dropChances=[80,		10]
	fairy.spawnRate=5

	## Electric Eel Mob
	eel=mob()
	eel.name="electric eel"
	eel.maxHealth=50
	eel.attack=20
	eel.spawnable=["ocean"]
	eel.disposition="neutral"
	eel.drops=["eel chunk"]
	eel.dropChances=[30]
	eel.spawnRate=10

	## Spider
	spider=mob()
	spider.name="spider"
	spider.maxHealth=10
	spider.attack=5
	spider.spawnable=["cave"]
	spider.drops=["string"]
	spider.dropChances=[90]
	spider.spawnRate=20

	## Death testy boi
	death=mob()
	death.name="death testy boi"
	death.maxHealth=9999999999999999
	death.attack=9999999999
	death.spawnable=["plains", "cave", "forest", "ocean"]
	death.disposition="aggressive"
	death.escapable=False

	mobs=[cow, goblin, skeleton, fairy, eel, spider]

	## Move mobs
	def moveMobs(self):
		for x in range(self.currentPlayer.location[0]-5, self.currentPlayer.location[0]+5):
			for y in range(self.currentPlayer.location[1]-5, self.currentPlayer.location[1]+5):
				loadedArea=self.getArea(x,y)
				for mob in loadedArea.mobs:
					## 25% chance of moving each turn
					if random.randint(0,3)==0:
						moveTo=self.getArea(x+random.randint(-1,1), y+random.randint(-1,1))
						## Mobs can only move to areas they can spawn in
						if moveTo.type.name in mob.spawnable:
							moveTo.mobs.append(mob)
							loadedArea.mobs.remove(mob)

	## Generate new area
	def newArea(self, x, y):
		created=self.area()
		created.mobs=[]
		created.items=[]
		if random.randint(1, self.biomeSize)==1:
			created.type = random.choice([biome for biome in self.biomes if biome.natural])
		else:
			## Pick type from an adjacent area
			## Create list of the coordinates of every adjacent tile
			adjacents = list(itertools.product(range(-1, 2), repeat=2))
			random.shuffle(adjacents)

			created.type=None
			for adjacent in adjacents:
				if self.getArea(x+adjacent[0], y+adjacent[1], noGen=True) is not None:
					created.type=self.getArea(x+adjacent[0], y+adjacent[1]).type

			## No adjacent areas, pick randomly
			if not created.type:
				created.type = random.choice([biome for biome in self.biomes if biome.natural])

		## Grow trees in forests
		if created.type.forested:
			created.trees=random.randint(1, 10)
		if created.type.underground:
			created.ore=[]
			## Add flint
			if random.randint(0,1)==0:
				for i in range(0, random.randint(0, 5)):
					created.ore.append("flint")
			## Add iron ore
			if random.randint(0,3)==0:
				for i in range(0, random.randint(0, 5)):
					created.ore.append("iron")
			## Add diamond ore
			if random.randint(0,10)==0:
				created.ore.append("diamond")
		## Spawn mobs
		for mob in self.mobs:
			if created.type.name in mob.spawnable and random.randint(0,100)<=mob.spawnRate:
				newMob=copy.deepcopy(mob)
				newMob.health=newMob.maxHealth
				created.mobs.append(newMob)
		## Set snowyness
		if created.type.snowy:
			created.snowy=True
		## Set grassyness
		if created.type.grassy:
			created.grassy=True
		return(created)
	def setArea(self, x, y, new):
		self.map[f"{x,y}"]=new
	def getArea(self, x, y, noGen=False):
		if f"{x,y}" in self.map:
			return self.map[f"{x,y}"]
		elif not noGen:
			self.setArea(x, y, self.newArea(x, y))
			return self.map[f"{x,y}"]
		else:
			return None
	## Save area as dictionary
	def areaToDict(self, area):
		areaDict={}
		areaDict["name"]=area.name
		areaDict["type"]=area.type.name
		areaDict["items"]=listToString(area.items)
		areaDict["ore"]=listToString(area.ore)
		areaDict["plants"]=listToString(area.plants)
		areaDict["trees"]=area.trees
		areaDict["snowy"]=str(area.snowy)
		areaDict["grassy"]=str(area.grassy)
		return areaDict
	## Save mob as dictionary
	def mobToDict(self, mob):
		mobDict={}
		mobDict["name"]=mob.name
		mobDict["health"]=mob.health
		mobDict["maxHealth"]=mob.maxHealth
		mobDict["drops"]=listToString(mob.drops)
		mobDict["dropChances"]=listToString(mob.dropChances)
		mobDict["disposition"]=mob.disposition
		mobDict["attack"]=str(mob.attack)
		mobDict["id"]=hash(str(mobDict)+str(time.time()))
		mobDict["escapable"]=str(mob.escapable)
		return mobDict
	## Save weapon as dictionary
	def weaponToDict(self, weapon):
		weaponDict={}
		if weapon is None:
			weaponDict["name"]="None"
			weaponDict["attack"]="None"
			weaponDict["accuracy"]="None"
			weaponDict["magic"]="False"
		else:
			weaponDict["name"]=weapon.name
			weaponDict["attack"]=weapon.attack
			weaponDict["accuracy"]=weapon.accuracy
			weaponDict["magic"]=str(weapon.magic)
		return weaponDict
	def save(self):
		if not os.path.exists(self.path):
			os.mkdir(self.path)
		## Save map
		mapFile=configparser.ConfigParser()
		mobFile=configparser.ConfigParser()
		for area in self.map:
			mapFile[area]=self.areaToDict(self.map[area])
			## Save mobs
			for mob in self.map[area].mobs:
				mobDict=self.mobToDict(mob)
				mobDict["location"]=area
				mobFile[mobDict["id"]]=mobDict
		mapFile.write(open(self.path + "/map.ini", "w"))
		mobFile.write(open(self.path + "/mobs.ini", "w"))

		## Save player
		playerFile=configparser.ConfigParser()
		playerDict={"locationX": self.currentPlayer.location[0],
					"locationY": self.currentPlayer.location[1],
					"inventory": listToString(self.currentPlayer.inventory),
					"health": self.currentPlayer.health,
					"maxHealth": self.currentPlayer.maxHealth,
					"spells": listToString(self.currentPlayer.spells),
					"mana": self.currentPlayer.mana,
					"maxMana": self.currentPlayer.maxMana}
		playerFile["player"]=playerDict
		playerFile["weapon"]=self.weaponToDict(self.currentPlayer.heldWeapon)
		playerFile.write(open(self.path + "/player.ini", "w"))
	## Load area from dictionary
	def areaFromDict(self, areaDict):
		## Get biome
		type=None
		for biome in self.biomes:
			if biome.name==areaDict["type"]:
				type=biome
		if type==None:
			print(f"WARNING: Unknown biome '{areaDict['type']}', generating a new area instead.")
			return(self.newArea())
		area=self.area()
		area.type=type
		## Get name
		if "name" in areaDict:
			area.name=areaDict["name"]
		else:
			area.name=""
		## Get items
		if "items" in areaDict:
			area.items=listFromString(areaDict["items"])
		else:
			area.items=[]
		## Get mobs
		if "mobs" in areaDict:
			area.mobs=listFromString(areaDict["mobs"])
		else:
			area.mobs=[]
		## Get ores
		if "ore" in areaDict:
			area.ore=listFromString(areaDict["ore"])
		else:
			area.ore=[]
		## Get plants
		if "plants" in areaDict:
			area.plants=listFromString(areaDict["plants"])
		else:
			area.plants=[]
		## Get trees
		if "trees" in areaDict:
			area.trees=int(areaDict["trees"])
		else:
			area.trees=0
		## Get snowyness
		if "snowy" in areaDict:
			area.snowy=areaDict["snowy"]=="True"
		else:
			area.snowy=False
		## Get grassyness
		if "grassy" in areaDict:
			area.grassy=areaDict["grassy"]=="True"
		else:
			area.grassy=False
		return area
	## Load mob from dictionary
	def mobFromDict(self, mobDict):
		mob=self.mob()
		mob.name=mobDict["name"]
		mob.health=int(mobDict["health"])
		mob.maxHealth=int(mobDict["maxHealth"])
		mob.disposition=mobDict["disposition"]
		mob.attack=int(mobDict["attack"])
		mob.drops=listFromString(mobDict["drops"])
		mob.dropChances=listFromString(mobDict["dropChances"])
		mob.escapable=(mobDict["escapable"]=="True")
		return mob
	## Load weapon from dictionary
	def weaponFromDict(self, weaponDict):
		if weaponDict["name"]=="None":
			weapon=None
		else:
			weapon=self.weapon()
			weapon.name=weaponDict["name"]
			weapon.attack=weaponDict["attack"]
			weapon.accuracy=weaponDict["accuracy"]
			weapon.magic=weaponDict["magic"]=="True"
		return weapon
	def load(self):
		## Make sure map exists
		if not os.path.exists(self.path):
			print("Error, world does not exist")
			exit()
		## Load map
		mapFile=configparser.ConfigParser()
		mapFile.read(self.path + "/map.ini")
		mobFile=configparser.ConfigParser()
		mobFile.read(self.path + "/mobs.ini")
		## Load map
		for areaName in mapFile.sections():
			self.map[areaName]=self.areaFromDict(mapFile[areaName])
		## Load mobs
		for mobId in mobFile.sections():
			self.map[mobFile[mobId]["location"]].mobs.append(self.mobFromDict(mobFile[mobId]))
		## Load player
		## Create new player
		self.currentPlayer=self.player()
		playerFile=configparser.ConfigParser()
		playerFile.read(self.path + "/player.ini")
		## Load location
		self.currentPlayer.location[0]=int(playerFile["player"]["locationX"])
		self.currentPlayer.location[1]=int(playerFile["player"]["locationY"])
		## Load inventory
		self.currentPlayer.inventory=listFromString(playerFile["player"]["inventory"])
		## Load weapon
		self.currentPlayer.heldWeapon=self.weaponFromDict(playerFile["weapon"])
		## Remove empty inventory items
		for item in self.currentPlayer.inventory:
			if item=="":
				self.currentPlayer.inventory.remove(item)
		## Load health
		self.currentPlayer.health=int(playerFile["player"]["health"])
		## Load max health
		self.currentPlayer.maxHealth=int(playerFile["player"]["maxHealth"])
		## Load mana
		self.currentPlayer.mana=int(playerFile["player"]["mana"])
		## Load max mana
		self.currentPlayer.maxMana=int(playerFile["player"]["maxMana"])
		## Load spells
		self.currentPlayer.spells=listFromString(playerFile["player"]["spells"])
