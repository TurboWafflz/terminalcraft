#!/bin/sh
MAIN_PATH="."
MODULE_PATH="./modules"
USER_PATH="."
INSTALL_DEPENDS=1

if [ $INSTALL_DEPENDS -eq "1" ]
then
	echo "Updating dependencies..."
	python3 -m ensurepip
	python3 -m pip install -r requirements.txt
fi

# If the script is being run in a terminal, just launch the game. Otherwise, open a terminal and launch the game.
if [ -t 1 ]
then
	python3 $MAIN_PATH/terminalCraft.py --module_path=$MODULE_PATH --user_path=$USER_PATH
else
	LAUNCH_COMMAND="python3 $MAIN_PATH/terminalCraft.py --module_path=$MODULE_PATH --user_path=$USER_PATH"
	xdg-terminal $LAUNCH_COMMAND || x-terminal-emulator -e $LAUNCH_COMMAND || exo-open --launch TerminalEmulator $LAUNCH_COMMAND
fi
