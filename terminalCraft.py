# TerminalCraft
# Copyright (C) 2021  Finian Wright
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import random
import colorama
import curses
import time
import platform
import copy
import sys
import shutil


## Add arguments
import argparse
all_args=argparse.ArgumentParser()
all_args.add_argument("--module_path", help="Path to load TerminalCraft Python modules from", default="./modules")
all_args.add_argument("--user_path", help="Path to save user data", default=".")
args = vars(all_args.parse_args())

args['module_path']=os.path.expanduser(args['module_path'])
args['user_path']=os.path.expanduser(args['user_path'])

## Load TerminalCraft Python modules
sys.path.insert(1, args["module_path"])
from tcWorld import world
import tcMinigames


## Windows users aren't fancy enough for readline
if not platform.system()=="Windows":
	import readline

def clearScreen():
	if platform.system()=="Windows":
		os.system("cls")
	else:
		os.system("clear")

def worldSelect():
	worldPath=f"{args['user_path']}/worlds"

	## If running portably, move old worlds into worlds directory
	if os.path.exists(".tcSrc"):
		legacyWorlds=[each for each in os.listdir() if each.endswith('.tcworld')]
		if len(legacyWorlds)>0:
			print("Migrating worlds to new location...")
			for toBeMoved in legacyWorlds:
				print(toBeMoved)
				shutil.move(toBeMoved, f"{worldPath}/{world}")
			print("Your worlds have been moved into the 'worlds' folder")
			print("[Press enter to continue]")
			input()
			clearScreen()

	## Create world path if it doesn't exist
	if not os.path.exists(worldPath):
		os.makedirs(worldPath, exist_ok=True)

	worlds=[]
	worlds += [each for each in os.listdir(worldPath) if each.endswith('.tcworld')]
	print(" _____                   _             _  ____            __ _   \n|_   _|__ _ __ _ __ ___ (_)_ __   __ _| |/ ___|_ __ __ _ / _| |_ \n  | |/ _ \ '__| '_ ` _ \| | '_ \ / _` | | |   | '__/ _` | |_| __|\n  | |  __/ |  | | | | | | | | | | (_| | | |___| | | (_| |  _| |_ \n  |_|\___|_|  |_| |_| |_|_|_| |_|\__,_|_|\____|_|  \__,_|_|  \__|")
	print(f"(Loading worlds from {worldPath})")
	print(colorama.Fore.GREEN + "Worlds:")
	for name in worlds:
		print(name[:-8])
	if len(worlds)==0:
		print(colorama.Fore.CYAN + "You currently have no worlds.")
	print()
	worldName=''
	while worldName.strip()=='':
		worldName=input(colorama.Style.BRIGHT + "Type the name of a world to load/create: " + colorama.Style.NORMAL)
	worldName=f"{worldPath}/{worldName}.tcworld"
	if os.path.exists(worldName):
		currentWorld=world()
		currentWorld.path=worldName
		currentWorld.load()
	else:
		currentWorld=world()
		currentWorld.currentPlayer=currentWorld.player()
		spawn=currentWorld.area()
		spawn.type=currentWorld.plains
		spawn.grassy=True
		currentWorld.setArea(0, 0, spawn)
		currentWorld.path=worldName
		## Give player wheat seeds
		currentWorld.currentPlayer.inventory.append("wheat seed")
		currentWorld.save()
	return currentWorld

def combat(currentWorld, enemyNumber):
	currentArea=currentWorld.getArea(currentWorld.currentPlayer.location[0], currentWorld.currentPlayer.location[1])
	mob=currentArea.mobs[enemyNumber]
	## Actual fight
	while True:
		## Action menu thingy
		print()
		print(f"Enemy Health: {currentArea.mobs[enemyNumber].health}/{currentArea.mobs[enemyNumber].maxHealth}")
		print(f"Health: {currentWorld.currentPlayer.health}/{currentWorld.currentPlayer.maxHealth}")
		print(f"Mana: {currentWorld.currentPlayer.mana}/{currentWorld.currentPlayer.maxMana}")
		print(colorama.Style.BRIGHT + "Actions:")
		print("Attack")
		print("Item")
		print("Flee")

		action=input(colorama.Style.BRIGHT + "What would you like to do this turn? " + colorama.Style.NORMAL)
		damage=0
		if action.lower()=="attack":
			## Attack
			if currentWorld.currentPlayer.heldWeapon is None:
				accuracy=10
				accuracy=tcMinigames.attackMinigame(speed=5, accuracy=accuracy, message=f"Fighting {mob.name}")
				maxDamage=4
				damage=round(maxDamage*((40-abs(accuracy))/40))
			else:
				if not currentWorld.currentPlayer.heldWeapon.magic:
					## Back and forth slider thingy minigame for player damage
					accuracy=currentWorld.currentPlayer.heldWeapon.accuracy
					accuracy=tcMinigames.attackMinigame(speed=5, accuracy=accuracy, message=f"Fighting {mob.name}")
					## Get maximum attack damage from held weapon
					maxDamage=float(currentWorld.currentPlayer.heldWeapon.attack)
					damage=round(maxDamage*((40-abs(accuracy))/40))
				else:
					print()
					print(colorama.Style.BRIGHT + "Spells:")
					## List spells that can be cast with the current weapon
					for spell in currentWorld.spells:
						if int(currentWorld.currentPlayer.heldWeapon.attack) >= int(spell.requiredPower) and spell.name in currentWorld.currentPlayer.spells:
							print(f"{spell.name} (Attack: {spell.attack}, Heal: {spell.heal}, Mana Cost: {spell.cost})")
					spellName=input(colorama.Style.BRIGHT + "Which would you like to cast? " + colorama.Style.NORMAL)
					castSpell=None
					## Find requested spell
					for testSpell in currentWorld.spells:
						if spellName==testSpell.name:
							castSpell=testSpell
					## If the requested spell exists, run accuracy minigame and calculate damage, else do no damage
					if not castSpell==None:
						if int(currentWorld.currentPlayer.mana)-int(castSpell.cost)>=0:
							currentWorld.currentPlayer.mana-=castSpell.cost
							currentWorld.currentPlayer.heal(castSpell.heal)
							if castSpell.attack > 0:
								## Back and forth slider thingy minigame for player damage
								if currentWorld.currentPlayer.heldWeapon is None:
									accuracy=10
								else:
									accuracy=currentWorld.currentPlayer.heldWeapon.accuracy
								accuracy=tcMinigames.attackMinigame(speed=5, accuracy=int(accuracy), message=f"Fighting {mob.name}")
								damage=round(castSpell.attack*((40-abs(accuracy))/40))
							print(colorama.Fore.MAGENTA + random.choice(castSpell.messages))
						else:
							print(f"You try to cast {castSpell.name} but realize you do not have enough mana!")
					else:
						print(f"You try to cast {spellName} but realize you have no idea what you're doing!")
		elif action.lower()=="flee":
			if mob.escapable:
				print("You ran away from the fight.")
				return "fled"
				break
			else:
				print("You try to escape, but can't find a way out")
		elif action.lower()=="item":
			availableItems=[]
			## Find all combatItems in the player's inventory
			for combatItem in currentWorld.combatItems:
				if combatItem.name in currentWorld.currentPlayer.inventory:
					availableItems.append(combatItem.name)
			## List items or display message about no items
			if len(availableItems)>0:
				print()
				print(colorama.Style.BRIGHT + "Items:")
				for availableItem in availableItems:
					print(availableItem)
				selectedItem=input(colorama.Style.BRIGHT + "Which would you like to use? " + colorama.Style.NORMAL)
				## Find selected item
				if selectedItem in availableItems:
					currentWorld.currentPlayer.inventory.remove(selectedItem)
					for combatItem in currentWorld.combatItems:
						if combatItem.name==selectedItem:
							actualItem=combatItem
					print(colorama.Fore.MAGENTA + random.choice(actualItem.messages))
					currentWorld.currentPlayer.restoreMana(actualItem.manaRestore)
					currentWorld.currentPlayer.heal(actualItem.heal)
					damage=actualItem.attack
				else:
					print(f"You try to use a {selectedItem} but realize you do not have one!")
			else:
				print("You search through your inventory for useful items but find none and realize you have wasted your entire turn looking.")

		else:
			print("You can't figure out what to do and just end up making a weird face at the enemy.")
		if damage>0:
			print(colorama.Fore.GREEN + f"You did {damage} damage.")
		time.sleep(1)
		currentArea.mobs[enemyNumber].health-=damage
		if currentArea.mobs[enemyNumber].health<=0:
			print(colorama.Fore.GREEN + colorama.Style.BRIGHT + "You killed the enemy!")
			i=0
			for i in range(len(currentArea.mobs[enemyNumber].drops)):
				if random.randint(0,100) < int(currentArea.mobs[enemyNumber].dropChances[i]):
					print(f"It dropped a {currentArea.mobs[enemyNumber].drops[i]}!")
					currentArea.items.append(currentArea.mobs[enemyNumber].drops[i])
			del currentArea.mobs[enemyNumber]
			break
		if currentArea.mobs[enemyNumber].disposition=="passive":
			if damage>0:
				print(f"The {mob.name} is helpless to fight back.")
			else:
				print(f"The {mob.name} stands there looking at you.")
		else:
			damage=random.randint(int(mob.attack*0.5), mob.attack)
			print(colorama.Fore.RED + f"The {mob.name} fights back and does {str(damage)} damage.")
			currentWorld.currentPlayer.health-=damage
			if currentWorld.currentPlayer.health < 0:
				print(colorama.Fore.RED + colorama.Style.BRIGHT + "You died!")
				currentWorld.currentPlayer.health=currentWorld.currentPlayer.maxHealth
				currentWorld.currentPlayer.location[0]=0
				currentWorld.currentPlayer.location[1]=0
				displayDescription=True
				break
			else:
				print(f"You currently have {str(currentWorld.currentPlayer.health)}/{str(currentWorld.currentPlayer.maxHealth)} health.")
		time.sleep(1)


def main():
	clearScreen()
	screen=curses.initscr()
	rows, cols = screen.getmaxyx()
	curses.endwin()
	if cols<80:
		print("This game requires your terminal to be at least 80 columns wide")
		exit()

	colorama.init(autoreset=True)
	currentWorld=worldSelect()
	clearScreen()
	print("Welcome to TerminalCraft")
	moveX=0
	moveY=0
	## Display description on first start
	displayDescription=True
	try:
		while True:
			## Make sure the player can move to the requested area before moving
			newArea=currentWorld.getArea(currentWorld.currentPlayer.location[0]+moveX, currentWorld.currentPlayer.location[1]+moveY)
			if newArea.type.ocean:
				if not "boat" in currentWorld.currentPlayer.inventory:
					moveX=0
					moveY=0
					print("You cannot go in that direction without a boat.")
					continue
				else:
					print("You are travelling in a boat.")
			currentArea=newArea
			currentWorld.currentPlayer.location[0]+=moveX
			currentWorld.currentPlayer.location[1]+=moveY
			## Only describe the player's current area if they have moved
			if moveX != 0 or moveY != 0 or displayDescription:
				if currentArea.name=="":
					print(colorama.Fore.GREEN + f"You are in a {currentArea.type.name} biome. ({currentWorld.currentPlayer.location[0]}, {currentWorld.currentPlayer.location[1]}) ", end='')
				else:
					print(colorama.Fore.GREEN + f"{currentArea.name} ({currentWorld.currentPlayer.location[0]}, {currentWorld.currentPlayer.location[1]}) ")
				if currentArea.type.grassy and not currentArea.grassy:
					print(colorama.Fore.CYAN + "The grass has been removed here")
				if currentArea.type.snowy and not currentArea.snowy:
					print(colorama.Fore.CYAN + "The snow has been cleared here")
				displayDescription=False
			moveX=0
			moveY=0

			## Move mobs
			currentWorld.moveMobs()
			## Grow plants
			currentWorld.growPlants()

			## Regen mana (Should probably have some way to improve regen but I can't think of a good way to do it now)
			currentWorld.currentPlayer.restoreMana(5)

			## Save world
			currentWorld.save()

			## Print mobs
			didCombat=False
			if len(currentArea.mobs)>0:
				print("Mobs: ")
				## List mobs with multiple of the same mob grouped together
				mobs=[]
				mobsCount=[]
				for mobNumber in range(len(currentArea.mobs)):
					mob=currentArea.mobs[mobNumber]
					if mob.disposition=="aggressive":
						print(colorama.Style.BRIGHT + f"A {mob.name} ambushes you!")
						if str(combat(currentWorld, mobNumber))=="fled":
							moveX=random.randint(-1,1)
							if moveX==0:
								moveY=random.randint(-1,1)
						displayDescription=True
						didCombat=True
						break
					mobIsInTheList=False
					for mobIterator in range(len(mobs)):
						if mobs[mobIterator]==mob.name:
							mobIsInTheList=True
							mobsCount[mobIterator] += 1
					if not mobIsInTheList:
						mobs.append(mob.name)
					mobsCount.append(1)
				for mob in range(len(mobs)):
					print(f"{mobsCount[mob]} {mobs[mob]}")
				print()
			## Skip this turn if the player was ambushed
			if didCombat:
				continue
			## Print number of trees if any
			if currentArea.trees>0:
				print(f"There are {currentArea.trees} trees here.", end='')
			print()
			## Print ores if any
			if len(currentArea.ore)>0:
				print("Ore: ")
				## List ores with multiple of the same item grouped together
				for ore in list(dict.fromkeys(currentArea.ore)):
					print(f"{currentArea.ore.count(ore)} {ore}")
				print()
			## Print plants
			if len(currentArea.plants)>0:
				print("Unripe plants: ")
				## List plants with multiple of the same item grouped together
				for plant in list(dict.fromkeys(currentArea.plants)):
					print(f"{currentArea.plants.count(plant)} {plant}")
				print()
			## Print items
			if len(currentArea.items)>0:
				print("Items on ground: ")
				## List items with multiple of the same item grouped together
				for item in list(dict.fromkeys(currentArea.items)):
					print(f"{currentArea.items.count(item)} {item}")
				print()

			action=input()
			## Convert action to lowercase and remove leading and trailing whitespace
			action=action.strip().lower()
			## Empty line to improve readability
			print()
			## Movement
			if action=="n" or action=="north":
				moveY=1
			elif action=="e" or action=="east":
				moveX=1
			elif action=="s" or action=="south":
				moveY=-1
			elif action=="w" or action=="west":
				moveX=-1
			## Details about area
			elif action=="look":
				print(f"You are in a {currentArea.type.name} biome at the coordinates ({currentWorld.currentPlayer.location[0]}, {currentWorld.currentPlayer.location[1]}). There are {currentArea.trees} trees here.")
			## Cut tree
			elif action=="cut tree":
				if currentArea.trees>0:
					print(colorama.Fore.CYAN + "You cut down a tree")
					wood=random.randint(3, 10)
					currentArea.trees-=1
					for i in range(wood):
						currentWorld.currentPlayer.inventory.append("wood")
				else:
					print(colorama.Fore.RED + "There are no trees to cut here")
			## View inventory
			elif action=="inventory":
				print("Inventory:")
				## List inventory with multiple of the same item grouped together
				for item in list(dict.fromkeys(currentWorld.currentPlayer.inventory)):
					print(f"{currentWorld.currentPlayer.inventory.count(item)} {item}")
				if currentWorld.currentPlayer.heldWeapon is not None:
					print(f"Weapon: {currentWorld.currentPlayer.heldWeapon.name}")
				print()
			## Craft items
			elif action.split(" ", 1)[0]=="craft":
				if len(action.split(" ", 1))==1:
					print("Recipes:")
					## List available recipes
					for recipe in currentWorld.recipes:
						print(recipe.item)
					print()
					item=input(colorama.Style.BRIGHT + "What would you like to craft? " + colorama.Style.NORMAL)
				else:
					item=action.split(" ", 1)[1]
				recipe=None
				## Find requested recipe
				for possibleRecipe in currentWorld.recipes:
					if possibleRecipe.item==item:
						recipe=possibleRecipe
				## Error if item cannot be found
				if recipe==None:
					print(colorama.Fore.RED + "Item not found")
					continue
				print()
				print("Recipe:")
				print(f"Gives: {recipe.item}")
				print(f"Ingredients:")
				## List ingredients with multiple of the same item grouped together
				for ingredient in list(dict.fromkeys(recipe.ingredients)):
					print(f"{recipe.ingredients.count(ingredient)} {ingredient}")
				print()
				wantsToCraft=input(colorama.Style.BRIGHT + "Do you want to craft this? (Y/n) " + colorama.Style.NORMAL)
				if not wantsToCraft.lower()=="n":
					craftable=True
					for ingredient in list(dict.fromkeys(recipe.ingredients)):
						if not currentWorld.currentPlayer.inventory.count(ingredient)>=recipe.ingredients.count(ingredient):
							craftable=False
					if craftable:
						print(colorama.Fore.CYAN + f"You crafted a {recipe.item}!")
						## Take ingredients
						for ingredient in recipe.ingredients:
							currentWorld.currentPlayer.inventory.remove(ingredient)
						## Give crafted item
						currentWorld.currentPlayer.inventory.append(recipe.item)
					else:
						print(colorama.Fore.RED + "You do not have the required materials!")
			## Mine
			elif action.split(" ")[0]=="mine":
				if len(action.split(" "))>1:
					## Mine ore if present
					if action.split(" ")[1] in currentArea.ore:
						if "pickaxe" in currentWorld.currentPlayer.inventory:
							print(colorama.Fore.CYAN + f"You mined some {action.split(' ')[1]}")
							currentArea.ore.remove(action.split(" ")[1])
							currentWorld.currentPlayer.inventory.append(action.split(" ")[1])
						else:
							if "crude pickaxe" in currentWorld.currentPlayer.inventory:
								print(colorama.Fore.RED + "You'll need more than a wooden pickaxe to mine ores!")
							else:
								print(colorama.Fore.RED + "You can't mine with your bare hands!")
					## Mine stone if underground
					elif action.split(" ")[1] == "stone" and currentArea.type.underground:
						## Mining stone requires a pickaxe
						if "crude pickaxe" in currentWorld.currentPlayer.inventory or "pickaxe" in currentWorld.currentPlayer.inventory:
							print(colorama.Fore.CYAN + "You mined some stone")
							currentWorld.currentPlayer.inventory.append("stone")
							if random.randint(0, 1000)==0:
								print(colorama.Fore.CYAN + "You also found a rock with some writing on it.")
								currentWorld.currentPlayer.inventory.append("strange rock")
						else:
							print(colorama.Fore.RED + "You can't mine with your bare hands!")
					else:
						print(colorama.Fore.RED + f"There is no {action.split(' ')[1]} to mine or {action.split(' ')[1]} cannot be mined.")
				else:
					print(colorama.Fore.RED + "You need to say what to mine!")
			## Place items
			elif action.split(" ", 1)[0]=="place":
				if len(action.split(" ", 1))>1:
					if action.split(" ", 1)[1] in currentWorld.currentPlayer.inventory:
						currentWorld.currentPlayer.inventory.remove(action.split(" ", 1)[1])
						currentArea.items.append(action.split(" ", 1)[1])
					else:
						print(colorama.Fore.RED + "You do not have that item!")
				else:
					print(colorama.Fore.RED + "You need to say what to place!")
			## Place all of a type of item
			elif action.split(" ", 1)[0]=="placeall":
				if len(action.split(" ", 1))>1:
					if action.split(" ", 1)[1] in currentWorld.currentPlayer.inventory:
						while action.split(" ", 1)[1] in currentWorld.currentPlayer.inventory:
							currentWorld.currentPlayer.inventory.remove(action.split(" ", 1)[1])
							currentArea.items.append(action.split(" ", 1)[1])
					else:
						print(colorama.Fore.RED + "You do not have that item!")
				else:
					print(colorama.Fore.RED + "You need to say what to place!")
			## Pick up items
			elif action.split(" ", 1)[0]=="take":
				if len(action.split(" ", 1))>1:
					if action.split(" ", 1)[1] in currentArea.items:
						currentArea.items.remove(action.split(" ", 1)[1])
						currentWorld.currentPlayer.inventory.append(action.split(" ", 1)[1])
						print(colorama.Fore.CYAN + f"You picked up a {action.split(' ', 1)[1]}")
					elif action.split(" ", 1)[1] == "all":
						print(colorama.Fore.CYAN + "You picked up all of the items")
						currentWorld.currentPlayer.inventory.extend(currentArea.items)
						currentArea.items=[]
					else:
						print(colorama.Fore.RED + "That item is not here!")
				else:
					print(colorama.Fore.RED + "You need to say what to take (or all)!")
			## Pick up all of a type of item
			elif action.split(" ", 1)[0]=="takeall":
				if len(action.split(" ", 1))>1:
					if action.split(" ", 1)[1] in currentArea.items:
						item = action.split(" ", 1)[1]
						while item in currentArea.items:
							currentArea.items.remove(action.split(" ", 1)[1])
							currentWorld.currentPlayer.inventory.append(action.split(" ", 1)[1])
						print(colorama.Fore.CYAN + f"You picked up all of the {action.split(' ', 1)[1]}")
					else:
						print(colorama.Fore.RED + "That item is not here!")
				else:
					print(colorama.Fore.RED + "You need to say what to take!")
			## Make current area farmable
			elif action=="cultivate":
				if "hoe" in currentWorld.currentPlayer.inventory:
					if currentArea.grassy:
						if currentArea.trees==0:
							print(colorama.Fore.CYAN + "The area is now farmable")
							currentArea.type=currentWorld.field
							displayDescription=True
						else:
							print(colorama.Fore.RED + "You need to cut down all the trees before you can cultivate this area.")
					elif currentArea.snowy:
						print(colorama.Fore.RED + "You must clear the snow first!")
					else:
						print(colorama.Fore.RED + "This area cannot be cultivated")
				else:
					print(colorama.Fore.RED + "You need a hoe to cultivate land")
			## Remove snow from current area
			elif action=="clear snow":
				if "shovel" in currentWorld.currentPlayer.inventory:
					if currentArea.snowy:
						print(colorama.Fore.CYAN + "The area is no longer snow covered")
						currentArea.snowy=False
						currentArea.grassy=True
						displayDescription=True
					else:
						print(colorama.Fore.RED + "There is no snow here")
				else:
					print(colorama.Fore.RED + "You need a shovel to clear snow!")
			## Plant seeds
			elif action.split(" ", 1)[0]=="plant":
				if currentArea.type.farmable:
					if len(action.split(" ", 1))==1:
						print("Plants:")
						for plant in currentWorld.plants:
							print(plant.name)
						print()
						toPlant=input(colorama.Style.BRIGHT + "What would you like to plant? " + colorama.Style.NORMAL)
					else:
						toPlant=action.split(" ", 1)[1]
					plantToPlant=None
					for plant in currentWorld.plants:
						if plant.name==toPlant:
							plantToPlant=plant
					if plantToPlant==None:
						print(colorama.Fore.RED + "Plant not found")
						continue
					if plantToPlant.seed in currentWorld.currentPlayer.inventory:
						print(colorama.Fore.CYAN + f"You planted a {plantToPlant.seed}")
						currentArea.plants.append(plantToPlant.name)
						currentWorld.currentPlayer.inventory.remove(plantToPlant.seed)
					else:
						print(colorama.Fore.RED + f"You do not have a {plantToPlant.seed}")
				else:
					print(colorama.Fore.RED + "You need to cultivate this area first.")
			## Cook items
			elif action.split(" ", 1)[0]=="cook":
				if "campfire" in currentArea.items:
					if len(action.split(" ", 1))==1:
						print("Recipes:")
						## List available recipes
						for recipe in currentWorld.cookingRecipes:
							print(recipe.item)
						print()
						item=input(colorama.Style.BRIGHT + "What would you like to cook? " + colorama.Style.NORMAL)
					else:
						item=action.split(" ", 1)[1]
					recipe=None
					## Find requested recipe
					for possibleRecipe in currentWorld.cookingRecipes:
						if possibleRecipe.item==item:
							recipe=possibleRecipe
					## Error if item cannot be found
					if recipe==None:
						print(colorama.Fore.RED + "Item not found")
						continue
					print()
					print("Recipe:")
					print(f"Gives: {recipe.item}")
					print(f"Ingredients:")
					## List ingredients with multiple of the same item grouped together
					for ingredient in list(dict.fromkeys(recipe.ingredients)):
						print(f"{recipe.ingredients.count(ingredient)} {ingredient}")
					print()
					wantsToCraft=input(colorama.Style.BRIGHT + "Do you want to cook this? (Y/n) " + colorama.Style.NORMAL)
					if not wantsToCraft.lower()=="n":
						craftable=True
						for ingredient in list(dict.fromkeys(recipe.ingredients)):
							if not currentWorld.currentPlayer.inventory.count(ingredient)>=recipe.ingredients.count(ingredient):
								craftable=False
						if craftable:
							accuracy=tcMinigames.timeMinigame(recipe.time)
							if accuracy<-10:
								print(colorama.Fore.RED + "You didn't cook it long enough! Try again")
							elif accuracy>10:
								## Take ingredients
								for ingredient in recipe.ingredients:
									currentWorld.currentPlayer.inventory.remove(ingredient)
								print(colorama.Fore.RED + "You burned it and ruined the ingredients!")
							else:
								print(colorama.Fore.CYAN + f"You cooked a {recipe.item}!")
								## Take ingredients
								for ingredient in recipe.ingredients:
									currentWorld.currentPlayer.inventory.remove(ingredient)
								## Give crafted item
								currentWorld.currentPlayer.inventory.append(recipe.item)
						else:
							print(colorama.Fore.RED + "You do not have the required materials!")
				else:
					print(colorama.Fore.RED + "You need a campfire to cook!")
			## Equip a weapon
			elif action.split(" ", 1)[0]=="equip":
				if len(action.split(" ", 1))==1:
					## Find all weapons in the player's inventory
					weapons=[]
					for item in currentWorld.currentPlayer.inventory:
						for weapon in currentWorld.weapons:
							if weapon.name==item:
								weapons.append(weapon)
					if len(weapons)==0:
						print("You do not have any weapons.")
						continue

					## List available weapons and their attack damage
					print("Weapons:")
					for i, weapon in enumerate(weapons):
						print(f"[{i+1}] {weapon.name} ({weapon.attack} Attack)")

					## Get and validate selection
					try:
						weaponNumber=int(input(colorama.Style.BRIGHT + "What do you want to equip? " + colorama.Style.NORMAL))-1
					except:
						print("Invalid selection")
						continue
					if weaponNumber+1 < 1 or weaponNumber+1 > len(weapons):
						print("Invalid selection")
						continue

					print(f"You equipped the {weapons[weaponNumber].name}.")
				else:
					item=action.split(" ", 1)[1]
					if not item in currentWorld.currentPlayer.inventory:
						print(f"You don't have a {item}!")
						continue
					found=False
					for weapon in currentWorld.weapons:
						if weapon.name==item:
							found=True
							break
					if not found:
						print(f"You can't use a {item} as a weapon!")
						continue
					print(f"You equipped the {weapon.name}.")
				## Place currently equipped weapon back in inventory
				if currentWorld.currentPlayer.heldWeapon is not None:
					currentWorld.currentPlayer.inventory.append(currentWorld.currentPlayer.heldWeapon.name)
					currentWorld.currentPlayer.heldWeapon=None
				## Move the weapon from the player's inventory to their held weapon slot
				currentWorld.currentPlayer.heldWeapon=weapon
				currentWorld.currentPlayer.inventory.remove(weapon.name)
			## Deequip
			elif action.split(" ", 1)[0]=="deequip":
				## Place currently equipped weapon back in inventory
				if currentWorld.currentPlayer.heldWeapon is not None:
					print(f"You place the {currentWorld.currentPlayer.heldWeapon.name} back in your inventory.")
					currentWorld.currentPlayer.inventory.append(currentWorld.currentPlayer.heldWeapon.name)
					currentWorld.currentPlayer.heldWeapon=None
				else:
					print("You do not currently have a weapon equipped!")

			## Study objects to learn spells
			elif action.split(" ", 1)[0]=="study":
				if len(action.split(" ", 1))==1:
					learnable=[]
					for spell in currentWorld.spells:
						if spell.learnFrom in currentWorld.currentPlayer.inventory and spell.name not in currentWorld.currentPlayer.spells:
							learnable.append(spell.learnFrom)
					print("You can learn new spells from the following items")
					for item in learnable:
						print(item)
					item=input(colorama.Style.BRIGHT + "Which would you like to study? " + colorama.Style.NORMAL)
					spell=None
					for testSpell in currentWorld.spells:
						if testSpell.learnFrom==item:
							spell=testSpell
				else:
					if action.split(" ", 1)[1] in currentWorld.currentPlayer.inventory:
						spell=None
						for testSpell in currentWorld.spells:
							if testSpell.learnFrom==action.split(" ", 1)[1]:
								spell=testSpell
					else:
						print("You do not have that item")
						continue
				if spell==None:
					print("You cannot learn a spell from that")
					continue
				if spell.name in currentWorld.currentPlayer.spells:
					print("You study the item but find nothing you don't already know")
					continue
				print(spell.learnMessage)
				currentWorld.currentPlayer.spells.append(spell.name)


			## Fight
			elif action.split(" ", 1)[0]=="fight":
				## Make sure there is actually something to fight
				if len(currentArea.mobs)==0:
					print("There is nothing to fight.")
					continue
				elif len(action.split(" ", 1))==1:
					## List mobs and their HP
					print("Mobs:")
					for i, mob in enumerate(currentArea.mobs):
						print(f"[{i+1}] {mob.name} ({mob.health} HP)")

					## Get and validate selection
					try:
						enemyNumber=int(input(colorama.Style.BRIGHT + "Type the number of the enemy you wish to fight: " + colorama.Style.NORMAL))-1
					except:
						print("Invalid selection")
						continue
					if enemyNumber+1 < 1 or enemyNumber+1 > len(currentArea.mobs):
						print("Invalid selection")
						continue
				else:
					target=action.split(" ", 1)[1]
					## List mobs matching specified name and their HP
					print(f"{target}s:")
					found=0
					for i, mob in enumerate(currentArea.mobs):
						if mob.name==target:
							print(f"[{i+1}] {mob.name} ({mob.health} HP)")
							foundNumber = i
							found +=1
					## Found no mobs with the specified name
					if found==0:
						print(f"There are no {target}s")
						continue

					## Get and validate selection
					try:
						## If there is only one enemy, figh it. Else, ask user which to fight
						if found==1:
							enemyNumber=foundNumber
						else:
							enemyNumber=int(input(colorama.Style.BRIGHT + "Type the number of the enemy you wish to fight: " + colorama.Style.NORMAL))-1
					except:
						print("Invalid selection")
						continue
					if enemyNumber+1 < 1 or enemyNumber+1 > len(currentArea.mobs):
						print("Invalid selection")
						continue
				combat(currentWorld, enemyNumber)
			elif action=="fish":
				if currentArea.type.ocean:
					if "fishing rod" in currentWorld.currentPlayer.inventory:
						accuracy=tcMinigames.fishingMinigame(2)
						if accuracy<2:
							print(colorama.Fore.GREEN + "You caught a fish!")
							currentWorld.currentPlayer.inventory.append("fish")
						elif accuracy<5:
							print(colorama.Fore.YELLOW + "The fish got away at the last second!")
						else:
							print(colorama.Fore.YELLOW + "I don't think that's how you fish.")
					else:
						print(colorama.Fore.RED + "You need a fishing rod to fish!")
				else:
					print(colorama.Fore.RED + "You cannot fish on land!")
			## Name the current area
			elif action=="name":
				print("What would you like to name the area? (leave blank to remove name)")
				currentArea.name=input()
				displayDescription=True
			## Wait without doing anything
			elif action=="wait":
				print(colorama.Fore.CYAN + "You wait for a bit")
				continue
			elif action in ["exit", "quit"]:
				print(colorama.Fore.CYAN + "Goodbye, your world has been saved for when you come back.")
				exit()
			else:
				print(colorama.Fore.RED + "I don't understand what you want me to do.")
	except (EOFError, KeyboardInterrupt):
		print(colorama.Fore.CYAN + "Goodbye, your world has been saved for when you come back.")
if __name__=="__main__":
	main()
