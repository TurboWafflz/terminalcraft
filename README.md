# TerminalCraft

TerminalCraft is a text based survival game with randomly generated worlds that are saved for future sessions.

**Note:** TerminalCraft is still fairly early in development and is missing a lot of features it needs to be an entertaining game. At this point it's just an experiment to see what a text based survival sandbox is like, but hopefully it will develop into more of a fully fledged game.

## You can now easily download and install TerminalCraft via Itch [Here](https://turbowafflz.itch.io/terminalcraft)!

## Supported Operating Systems
- Linux
- FreeBSD
- Windows (Rarely tested, generally in Wine, but seems to work)
- Any other OS that can run Python 3 should work but is untested

## Running
- **Linux**: Run `terminalCraft.sh`
- **FreeBSD**: Run `terminalCraft.sh`
- **Windows**: Run `terminalCraft.bat`
- **All other operating systems**: Manually run `terminalCraft.py` with Python 3

## Installation
If you want to install the game instead of just running the portable version, you can do so with `make install` on most Unix-like operating systems and uninstall with `make uninstall`. Packages are probably coming soon.

## Gameplay
### Exploring
- Move around the randomly generated world
- Cut trees in forests
- Craft wood into a boat to travel through oceans

### Farming
- Craft wood into a hoe to cultivate fields
- Plant seeds to grow wheat
- Craft wheat into dough

### Fishing
- Craft wood and string into a fishing rod
- Craft wood into a boat
- Go to an ocean
- Fish

### Cooking
- Craft a campfire from wood and flint
- Place the campfire somewhere
- Cook the dough to make bread

### Mining
- Craft wood into a crude pickaxe to mine stone
- Craft wood and stone into a pickaxe to mine ores

### Magic
- Craft a wand from leather, wood, and a goblin tooth
- Learn spells from special items found in the world (hint: a good starting spell can be obtained from a mob in a cave)
- Equip your wand and use it in combat
- Make a rod to cast more powerful spells

### Mobs
- Mobs in a 5x5 square around the player move around each turn
- Turn based combat with an accuracy minigame to determine damage dealt




## Current commands
*Arguments in square brackets are optional*
### Movement
- `n/e/s/w` - move in the respective direction

### Info
- `look` - Get information about the current area
- `name` - Name the current area

### Items
- `inventory` - View your inventory
- `take <item>` - Pick up an item from the ground. If `all` is specified as the item, every item in the area is picked up.
- `takeall <item>` - Take all of the specified items from the ground
- `place <item>` - Place an item on the ground in the current area
- `placeall <item>` - Place all of the specified items you have in your inventory
- `equip [weapon]` - Equip a weapon from your inventory
- `deequip` - De-equip your current weapon


### Farming
- `cultivate` - Turn the current area into a field
- `clear snow` - Clear the snow in a snowy area so it can be cultivated
- `plant [plant]` - Plant a plant
- `cut tree` - Cut a tree if one is present in the current area

## Fishing
- `fish` - Fish

### Crafting
- `craft [item]` - Craft an item from materials in your inventory
- `cook [resulting item]` - Cook something

### Combat
- `fight [mob]` - Fight a mob that is in the current area

### Mining
- `mine <ore name/stone>` - Mine an ore if available, or stone. Stone can be mined in any underground biome.

## Magic
- `study [object]` - Study an object to learn a spell from it

### Other
- `wait` - Wait one turn
- `exit` or `quit` - Leave the game
