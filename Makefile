default:
	@echo "Error: Please specify a target"

makeBuildDir:
	mkdir -p .build

getInstallDir:
	@test -d "/usr/" || (echo "You cannot install TerminalCraft this way on your OS"; exit 1)
ifeq ($(shell id -u),0)
installDir:=/usr/local/share/terminalCraft
launcherDir:=/usr/local/bin/
launcherName:=terminalCraft
else
installDir:=~/.local/share/terminalCraft
launcherDir:=~/.local/bin/
launcherName:=terminalCraft
endif

installDepends: requirements.txt
	# Make sure Python is installed
	@python3 --version || (echo "Please install Python 3 and try installing TerminalCraft again"; exit 1)
	# Make sure pip is installed, try to install it if not
	@python3 -m pip --version || python3 -m ensurepip || (echo "Please install Pip for Python 3 and try installing TerminalCraft agan"; exit 1)
	# Install required Python modules
	python3 -m pip install -r requirements.txt

buildLauncher: getInstallDir makeBuildDir
	cp terminalCraft.sh .build/.customizedLauncher.sh
	# Update paths
	sed -i "s+MAIN_PATH=\(.*\)+MAIN_PATH=$(installDir)/+g" .build/.customizedLauncher.sh
	sed -i "s+MODULE_PATH=\(.*\)+MODULE_PATH=$(installDir)/modules+g" .build/.customizedLauncher.sh
	sed -i "s+USER_PATH=\(.*\)+USER_PATH=~/.config/terminalCraft+g" .build/.customizedLauncher.sh
	# Disable dependency installing since we already did it
	sed -i "s+INSTALL_DEPENDS=\(.*\)+INSTALL_DEPENDS=0+g" .build/.customizedLauncher.sh

buildStructure: buildLauncher makeBuildDir
	# Make directory structure
	mkdir -p .build/bin/
	mkdir -p .build/share/
	# Copy scripts and launcher
	cp terminalCraft.py .build/share/
	cp -r modules .build/share/
	cp .build/.customizedLauncher.sh .build/bin/terminalCraft

install: getInstallDir installDepends buildStructure
	@echo "Installing to $(installDir)"
	mkdir -p $(installDir)
	cp -r .build/share/* $(installDir)
	mkdir -p $(launcherDir)
	cp .build/bin/terminalCraft $(launcherDir)/$(launcherName)

uninstall:
	@echo "Are you sure you want to uninstall, deleting $(installDir) and $(launcherPath)? (y/N)"
	@read confirm; test "$$confirm" = "y" || (echo "Cancelled"; exit 1)
	rm -rf $(installDir)
	rm $(launcherPath)

clean:
	rm -rf .build
